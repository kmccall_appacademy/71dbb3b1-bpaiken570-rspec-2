# 01_performance_monitor

require 'time'

def measure(num = 1)
  start = Time.now
  num.times { yield }
  ((Time.now - start) / num)
end
